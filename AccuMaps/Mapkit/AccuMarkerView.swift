//
//  AccuMarkerView.swift
//  AccuMaps
//
//  Created by Tim Grable on 6/30/20.
//  Copyright © 2020 Tim Grable. All rights reserved.
//

import Foundation
import MapKit

class AccuMarkerView: MKMarkerAnnotationView {
    override var annotation: MKAnnotation? {
        willSet {
            canShowCallout = true
            calloutOffset = CGPoint(x: 0, y: 0)
            rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
            isDraggable = true
            markerTintColor = UIColor.random()
        }
    }
}
