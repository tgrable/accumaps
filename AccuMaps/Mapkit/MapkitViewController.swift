//
//  MapkitViewController.swift
//  AccuMaps
//
//  Created by Tim Grable on 6/29/20.
//  Copyright © 2020 Tim Grable. All rights reserved.
//

import UIKit
import MapKit
import Combine

class MapkitViewController: UIViewController, MKMapViewDelegate {

    private var mapView = MKMapView(frame: .zero)
    private let locationService = LocationService()
    private var cancellables = Set<AnyCancellable>()
    private var jobLocations: [JobLocation] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationService.getCurrentLocation()
        mapView.register(AccuMarkerView.self, forAnnotationViewWithReuseIdentifier: MKMapViewDefaultAnnotationViewReuseIdentifier)

        updateSubviews()
        setupSubviews()
        setupConstraints()
    }
    
    private func updateSubviews() {
        view.addSubview(mapView)
    }
    
    private func setupSubviews() {
        view.backgroundColor = .white
        
        mapView.translatesAutoresizingMaskIntoConstraints = false
        mapView.backgroundColor = .blue
        mapView.delegate = self
        
        let singleTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(MapkitViewController.foundTap(_:)))
        singleTapRecognizer.delegate = self
        mapView.addGestureRecognizer(singleTapRecognizer)
        
        let address = "705 3rd Street, STE 380, Beloit, WI 53511"
        geoCodeUsingAddress(address: address)
        
        locationService.$userLocation
            .receive(on: DispatchQueue.main)
            .sink(receiveValue: { [weak self] location in
                guard let uwSelf = self, let uwLocation = location else { return }
                print(uwLocation)
                uwSelf.mapView.centerToLocation(uwLocation)
            })
            .store(in: &cancellables)
    }
    
    private func setupConstraints() {
        NSLayoutConstraint.activate([
           mapView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
           mapView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
           mapView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
           mapView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
       ])
    }

    @objc
    func foundTap(_ recognizer: UITapGestureRecognizer) {
        let guide = view.safeAreaInsets
        let point = recognizer.location(in: mapView)
        let adjustedPoint = CGPoint(x: point.x, y: point.y + guide.top)
        let tapPoint = mapView.convert(adjustedPoint, toCoordinateFrom: view)
        let newLocation = JobLocation(title: "Something", locationName: "Here", coordinate: tapPoint)
        mapView.addAnnotation(newLocation)
    }
    
    func geoCodeUsingAddress(address: String) {
        let geoCoder = CLGeocoder()
        geoCoder.geocodeAddressString(address) { (placemarks, error) in
            let placemark = placemarks?.first
            guard let lat = placemark?.location?.coordinate.latitude, let lon = placemark?.location?.coordinate.longitude else { return }
            print("Lat: \(lat), Lon: \(lon)")
            guard let uwLocation = placemark?.location else { return }
            self.reverseGeocodeLocation(location: uwLocation)
        }
    }
    
    func reverseGeocodeLocation(location: CLLocation) {
        let geoCoder = CLGeocoder()
        geoCoder.reverseGeocodeLocation(location) { (placemarks, error) in
            guard let placemark = placemarks?.first else { return }
            print(placemark)
        }
    }
}

private extension MKMapView {
    func centerToLocation(_ location: CLLocation, regionRadius: CLLocationDistance = 100) {
        let coordinateRegion = MKCoordinateRegion(center: location.coordinate,
                                              latitudinalMeters: regionRadius,
                                              longitudinalMeters: regionRadius)
        setRegion(coordinateRegion, animated: true)
    }
}

extension MapkitViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        return !(touch.view is MKPinAnnotationView)
    }
}
