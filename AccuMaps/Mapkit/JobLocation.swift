//
//  JobLocation.swift
//  AccuMaps
//
//  Created by Tim Grable on 6/30/20.
//  Copyright © 2020 Tim Grable. All rights reserved.
//

import Foundation
import MapKit

class JobLocation: MKPointAnnotation {
    init(title: String?, locationName: String?, coordinate: CLLocationCoordinate2D) {
        super.init()
        
        self.title = title
        self.subtitle = locationName
        self.coordinate = coordinate
    }
}
