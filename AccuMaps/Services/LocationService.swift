//
//  LocationService.swift
//  AccuMaps
//
//  Created by Tim Grable on 6/30/20.
//  Copyright © 2020 Tim Grable. All rights reserved.
//

import Foundation
import CoreLocation
import Combine

class LocationService: NSObject, CLLocationManagerDelegate {

    private let locationManager = CLLocationManager()
    
    var authorized: Bool { return CLLocationManager.authorizationStatus() == .authorizedWhenInUse }
    
    @Published var userLocation: CLLocation?

    override init() {
        super.init()
        locationManager.distanceFilter = kCLDistanceFilterNone
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
    
        locationManager.delegate = self
        getCurrentLocation()
    }
    
    func getCurrentLocation() {
        if authorized {
            locationManager.startUpdatingLocation()
        } else {
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedAlways || status == .authorizedWhenInUse {
            getCurrentLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        userLocation = locations.first
        locationManager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("error retreiving location \(error.localizedDescription)")
    }
}
